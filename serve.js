const express = require('express');
const open = require('open');
const app = express();

const fs = require('fs');

const PORT = 3232;

app.set('view engine', 'ejs');
app.use(express.static('./react-scripts'));
app.use(express.static('./css'));

app.get('/*', (request, response) => {
    fs.readdir('./react-scripts', (err, files) => {
        if (err) files = [];
        fs.readdir('./react-scripts/components', (err, f) => {
            if (!err) files.push(...f.map((n) => `components/${n}`));
            fs.readdir('./react-scripts/routes', (err, f) => {
                if (!err) files.push(...f.map((n) => `routes/${n}`));
                files = files.filter((f) => f !== 'app.jsx' && f.match('.jsx'));
                response.render('index', { files });
            });
        });
    });
});

app.listen(PORT, () => console.log(`App listen on port: ${PORT}`));
open(`http://localhost:${PORT}`);
