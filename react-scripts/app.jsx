const { BrowserRouter: Router, Switch, Route, Link } = ReactRouterDOM;

function App() {
    return (
        <Router>
            <div className="container">
                <Left />
                <div className="right">
                    <Switch>
                        <Route path="/about">
                            <About />
                        </Route>
                        <Route path="/users">
                            <Users />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>
            </div>
        </Router>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
