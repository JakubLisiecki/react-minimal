function Left() {
    return (
        <div className="left">
            <div className="pure-menu custom-restricted-width">
                <span class="pure-menu-heading">HI!</span>
                <ul className="pure-menu-list">
                    <li className="pure-menu-item">
                        <Link className="pure-menu-link" to="/">
                            Home
                        </Link>
                    </li>
                    <li className="pure-menu-item menu-item-divided">
                        <Link className="pure-menu-link" to="/about">
                            About
                        </Link>
                    </li>
                    <li className="pure-menu-item">
                        <Link className="pure-menu-link" to="/users">
                            Users
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    );
}
