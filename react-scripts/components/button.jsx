const { useState } = React;

function Button(props) {
    const [state, _setState] = useState({ title: 'bubbu', nono: 'xxx' });
    function setState(stateSlice) {
        _setState({ ...state, ...stateSlice });
    }
    return <button>{state.title}</button>;
}
